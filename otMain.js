const _MS_PER_DAY = 1000 * 60 * 60 * 24;

var siteUrl = '/collab/ts/cat/otbk';
var dailyUnitConnections = 4;

var todaysHighlight = 'lavenderBlush';    // Mark today with this color in calendar
var weekEndMarkerClass = 'weekEndDay';

var detailedListName = 'OTBookings';      // custom list to store detailed reservation data: 
                                          // Date is primary key, one column per language unit, storing data such: 'Slots|UserName|Time' (2|margama|16:10')

const languageUnits = ['BG','CS','DA','DE','EL','EN','ES','ET','FI','FR','GA','HR','HU','IT','LT','LV','MT','NL','OU','PL','PT','RO','SK','SL','SV'];

//*************************************************** TIME PROCESSING FOR CALENDAR ************************************************************** */

function get_CtMonth_FirstDay(returnString) {
    
    var cDate = new Date();

    var fdDate = new Date(cDate.getFullYear() + "-" + (parseInt(cDate.getMonth()) + parseInt(1)).toString());
    var fDay = fdDate.getDay();

    var Days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    
    if (typeof(returnString) == 'undefined' || returnString == null) {
        return fDay; 
    } else if(returnString == true || returnString.toLowerCase() == "returnstring") {
        return Days[fDay];
    } else {
        return fDay;
    }
}

function get_Month_FirstDay(month, returnString) {

  var cDate = new Date();

  var fdDate = new Date(cDate.getFullYear() + "-" + (parseInt(month)).toString());
  var fDay = fdDate.getDay();

  var Days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
  
  if (typeof(returnString) == 'undefined' || returnString == null) {
      return fDay; 
  } else if(returnString == true || returnString.toLowerCase() == "returnstring") {
      return Days[fDay];
  } else {
      return fDay;
  }
}

// NEED this to know where to end current month calendar populating
function daysInMonth(year, month) {
    return new Date(year, month, 0).getDate();
}

function daysInCtMonth() {
  return new Date(getCurrentYear(), getCurrentMonth() + 1, 0).getDate();
}

// DITTO, less parameters
function daysInCtYrMonth(month) {
    return new Date(new Date().getFullYear(), month, 0).getDate();
}

// Current Year is displayed on page, above calendar
function getCurrentYear() {
  return new Date().getFullYear();
}

// compute non-reserved spare connections for a certain day
function getSparesForDay() {

}

async function writeLoanerConnections() {

  var loanerDates = getNextTwo_WorkDays();
  var loanerCells = getCellsForDates(loanerDates);

  for (var k = 0; k < loanerCells.length; k++) {
    if (loanerCells[k] != null) {
      document.querySelector('#' + loanerCells[k].id + ' .spareConnections').innerText = '+' + await getRemaining_SpareConnections(loanerDates[k]);
    }
  }
}

function getCellsForDates(DatesArray) {
  var result = [];
  for (var j = 0; j < DatesArray.length; j++) {
    var c = getCellForDate(DatesArray[j]);
    result.push(c);
  }
  return result;
}

// In case the requested month is displayed currently, return table cell (td) for respective day, or null otherwise
function getCellForDate(RequestedDate) {
  var reqMonth = RequestedDate.split('/')[1];
  var reqMonthName = getMonthName(parseInt(reqMonth - 1));
  
  if (reqMonthName == getDisplayedMonth('returnText')) {
    var offset = parseInt(getMonthDetails().firstDay - 1);
    var reqDate = parseInt(RequestedDate.split('/')[2]);
    return document.querySelector('#' + 'dateCell_' + parseInt(offset + reqDate)); 
  } else {
    return null;
  }
}

function getDisplayedYear() {
  return document.querySelector('#otb_Year').innerText;
}

// DITTO for current month. Also used elsewhere in code
function getCurrentMonth(returnText) {
  var ctMonth = new Date().getMonth();
  if (returnText) {
    return getMonthName(ctMonth);
  } else {
    return ctMonth;
  }
}

function getMonthName(month) {
  var Months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
  if (typeof(month) == 'string') { 
    var iMonth = parseInt(month);
  } else {
    iMonth = month;
  }
  return Months[iMonth];
}

function getDateForDay(Day, sp) {
  
  var ctDay = formatNumber(Day, '00');
  var ctMonth = formatNumber(getDisplayedMonth() + 1, '00');  //1-based for human readable dates
  var ctYear = getCurrentYear();

  if (sp == undefined) { sp = '/' }
  return ctYear + sp + ctMonth + sp + ctDay;
}

// using YYYY/MM/DD format, separator at choice
function getTodaysDate(sp) {
  
  var ctDay = formatNumber((new Date().getDate()), '00');
  var ctMonth = formatNumber(getCurrentMonth() + 1, '00'); //1-based for human readable dates
  var ctYear = getCurrentYear();

  if (sp == undefined) { sp = '/' }
  return ctYear + sp + ctMonth + sp + ctDay;
}

function getCurrentTime() {
  var today = new Date();
  return formatNumber(today.getHours(), '00') + ':' + 
          formatNumber(today.getMinutes(), '00') + ':' + 
          formatNumber(today.getSeconds(), '00');
}

function formatNumber(number, format) {
  var output = number + '';
  
  if (format.match(/0{1,}/) != null) {
    while (output.length < format.length) {
      output = '0' + output;
    }
  } else if (format.match(/#{1,}/) != null) {
    while (output.substr(1, 1) == '0') {
      output = output.substr(2);
    }
  }
  return output;
}

function getToday() {
  if (getCurrentMonth('returnText') == document.querySelector('#otb_Month').innerText) {
    
    var firstDay = getMonthDetails(getCurrentMonth() + 1).firstDay;
    var todaysID = 0;

    if (firstDay != 1) { 
      todaysID = firstDay - 1 + new Date().getDate();
    } else {
      todaysID = new Date().getDate();
    }
    var targetCell = document.querySelector('#mainDate_' + todaysID);

    if (targetCell != null) {
      return targetCell;
    } else {
      return null;
    }
  } else {
    return null;
  }
}

function getNextTwo_WorkDays(returnObjects, sp) {
  var daysArray = [];
  var tomorrow = getTomorrowsDate();

  if (tomorrow.getDay() == 6) {
    var d1 = getDateForDaysOver(3);
    var d2 = getDateForDaysOver(4);
  } else if (tomorrow.getDay() == 0) {
    var d1 = getDateForDaysOver(2);
    var d2 = getDateForDaysOver(3);
  } else {
    var d1 = getDateForDaysOver(1);
    var d2 = getDateForDaysOver(2);
  }

  if (sp == undefined) { sp = '/' }

  if (returnObjects == undefined) {
    daysArray.push(d1.getFullYear() + '/' + formatNumber(parseInt(d1.getMonth() + 1), '00') + '/' + formatNumber(d1.getDate(), '00'));
    daysArray.push(d2.getFullYear() + '/' + formatNumber(parseInt(d2.getMonth() + 1), '00') + '/' + formatNumber(d2.getDate(), '00'));
  } else {
    daysArray.push(d1);
    daysArray.push(d2);
  }
  

  return daysArray;
}

function markToday() {

  // for current month calendar, mark Today
  if (getCurrentMonth('returnText') == document.querySelector('#otb_Month').innerText) {
    var monthD = getMonthDetails();
    var targetCell = document.querySelector('#dateCell_' + (monthD.firstDay - 1 + new Date().getDate()));
    if (targetCell != null) {
      markCellAsToday(targetCell, 'mark');
    } else {
      console.error('markToday: Could not retrieve target table cell for current day! (' + todaysDay + ')');
    }
  } else {
    // but remove it for other months
    targetCell = document.querySelector('.todaysDate');
    if (targetCell != null && targetCell != undefined) {
      markCellAsToday(targetCell, 'unmark');
    }
  }
}

function markCellAsToday(tableCell, operation) {
  if (operation == 'mark') {
    tableCell.classList.add('todaysDate');
    var todaysDay = new Date().getDate();
    // if today falls in a week-end, the cell also 'contains' the bottom cells...
  } else {
    tableCell.classList.remove('todaysDate');
  }
}

function getCtMonthsSaturdays() {
  
  var fDay = get_CtMonth_FirstDay();        // 1-based day of the week of first of current month
  var oSaturday = 1 + 6 - parseInt(fDay);   // number of month's first Saturday's 

  var Saturdays = [];
  Saturdays.push(oSaturday);

  var daysM = daysInCtYrMonth(parseInt(new Date().getMonth()) + 1);

  while (oSaturday + 7 <= daysM) {
    oSaturday = oSaturday + 7;
    Saturdays.push(oSaturday);
  }

  return Saturdays;
}

function getCtMonthsSundays() {
  
  var fDay = get_CtMonth_FirstDay();        // 1-based day of the week of first of current month
  // Sunday comes back as 0 from the above function!
  if (fDay == 0) {
    fDay = 7;       // force-correct for RO mentality!
  }
  var oSunday = 1 + 7 - parseInt(fDay);   // number of month's first Saturday's 

  var Sundays = [];
  Sundays.push(oSunday);

  var daysM = daysInCtYrMonth(parseInt(new Date().getMonth()) + 1);

  while (oSunday + 7 <= daysM) {
    oSunday = oSunday + 7;
    Sundays.push(oSunday);
  }

  return Sundays;
}

function getMonthsSaturdays(month) {
  
  var fDay = get_Month_FirstDay(month);       // 1-based day of the week of first of current month
  var oSaturday = 1 + 6 - parseInt(fDay);     // number of month's first Saturday's 

  var Saturdays = [];
  Saturdays.push(oSaturday);

  var daysM = daysInMonth(new Date().getFullYear(), parseInt(month));

  while (oSaturday + 7 <= daysM) {
    oSaturday = oSaturday + 7;
    Saturdays.push(oSaturday);
  }

  return Saturdays;
}

function getMonthsSundays(month) {
  
  var fDay = get_Month_FirstDay(month);       // 1-based day of the week of first of current month
  
  // Sunday comes back as 0 from the above function!
  if (fDay == 0) {
    fDay = 7;       // force-correct for RO mentality!
  }
  var oSunday = 1 + 7 - parseInt(fDay);     // number of month's first Saturday's 

  var Sundays = [];
  Sundays.push(oSunday);

  var daysM = daysInMonth(new Date().getFullYear(), parseInt(month));

  while (oSunday + 7 <= daysM) {
    oSunday = oSunday + 7;
    Sundays.push(oSunday);
  }

  return Sundays;
}

// function to paint week-end calendar days in gray
function getCtMonthsWeekEnds() {
  var weekEndDays = getCtMonthsSaturdays();
  weekEndDays = weekEndDays.concat(getCtMonthsSundays());

  return weekEndDays.sort(function(a, b){return a - b});
}

function getMonthWeekEnds(month) {
  var weekEndDays = getMonthsSaturdays(month);
  weekEndDays = weekEndDays.concat(getMonthsSundays(month));

  return weekEndDays.sort(function(a, b){return a - b});
}

function markCtWeekEnds() {
  
  var ctWeekEnds = getCtMonthsWeekEnds();
  var idx = 0;

  while (idx < ctWeekEnds.length) {
    document.querySelector('#dateCell_' + ctWeekEnds[idx]).classList.add(weekEndMarkerClass);
    idx = idx + 1;
  }
}

function isDayInWeekEnd(Day) {
  
  var ctYear, ctMonth
  ctYear = new Date().getFullYear();
  ctMonth = parseInt(getDisplayedMonth()) + 1;

  var targetDaysWeekDay = new Date(ctYear + '-' + ctMonth + '-' + Day).getDay();

  return (targetDaysWeekDay == 6 || targetDaysWeekDay == 0)

}

/* *********************************************** LANGUAGE UNIT SELECTION "DROPDOWN" CODE ****************************************************/

// function to create and return a new event handler function
var displaySelectedLanguage = function(ev) {
    
    var clickedLink = ev.target;
    document.querySelector('#lgSelected').innerText = clickedLink.innerText;
    //document.querySelector('.dropdown-content').style.display = 'none';
    localStorage.setItem("OTB_SelectedLanguage", clickedLink.innerText);
    return this;
}


// Function determines the target hyperlinks on page with a specific class and attaches an event listener to each of them
// thus enabling the "unhide and navigate to" functionality described below, in "unHideAndNavigateTo" function :)
var attach_LanguageSelect_Event = function (tgClassName) {
  
  var linksToProcess = document.getElementsByClassName(tgClassName);
  
  if (linksToProcess.length > 0) {
    
    for (var j = 0; j < linksToProcess.length; j++) { 
      
      var localj = j;
      //console.log('attach_LanguageSelect_Event: created localj = ' + localj);
      var link_href = linksToProcess[localj];
      //console.log('attach_LanguageSelect_Event: link_href is ' + link_href.innerText);

      //links skipping to ids (bookmarks) on same page, href = "#id"
      link_href.addEventListener('click', function(ev) {
                                            ev.preventDefault;
                                            displaySelectedLanguage(ev);
                                            populateCalendar(getDisplayedMonth() + 1);
                                            //forceRedraw(document.getElementById('monthOTCalendar'));    // also display currently displayed month of current unit
                                          }
                                );
    }
  }
  return this;
};


//*********************************************** RESERVATION CODE *****************************************************************************/
//
//******************************* */ Code to store current reservaton (including getting previous unit reservation) ********************************* 
//
// Function to add click event handler for all cells of calendar table. Calling storeClickedReservationAsync.
var attach_BookOTw_ClickEvent = function(tgClassName) {
  
  var targetCells = document.getElementsByClassName(tgClassName);

  if (targetCells.length > 0) {

    for (var j = 0; j < targetCells.length; j++) {
      
      if (!targetCells[j].classList.contains(weekEndMarkerClass)) {

        targetCells[j].addEventListener('click', function(ev) {
                                                      //ev.preventDefault;
                                                      storeClickedReservationAsync(ev);
                                                    }
                                          );
      }
    }
  }
  return this;
};

var attach_ShowReserve_Tooltip_MouseoverEvent = function() {
  
  var targetCells = document.getElementsByClassName('dateCell');

  if (targetCells.length > 0) {

    for (var j = 0; j < targetCells.length; j++) {
      
      if (!targetCells[j].classList.contains(weekEndMarkerClass)) {

        targetCells[j].addEventListener('mouseenter', function(ev) {
                                                      //ev.preventDefault;
                                                      display_Reserve_InvitationTip(ev);
                                                    }
                                          );
        targetCells[j].addEventListener('mouseleave', function(ev) {
                                                      //ev.preventDefault;
                                                      hide_Reserve_InvitationTip(ev);
                                                    }
                                          );
        
      }
    }
  }
  return this;
};

function getDateForDaysOver(i) {
  var today = new Date();
  return new Date(today.setDate(today.getDate() + i));
}

function getTomorrowsDate() {
  var today = new Date();
  return new Date(today.setDate(today.getDate() + 1));
}

// a and b are javascript Date objects
function dateDiffInDays(a, b) {
  // Discard the time and time-zone information.
  const utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
  const utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

  return Math.floor((utc2 - utc1) / _MS_PER_DAY);
}

function testDiffInDays(TxtDate1, TxtDate2) {
  
  var a = new Date(TxtDate1);
  var b = new Date(TxtDate2);

  return dateDiffInDays(a, b);
}

function testIsInThePast_ClickedCell(DayInDisplayedMonth) {
  
  var todaysDate = new Date(getTodaysDate());
  var clickedDate = new Date(getDateForDay(DayInDisplayedMonth));

  /*if (dateDiffInDays(todaysDate, clickedDate) < 0) {
    return true;
  } else {
    return false;
  } */
  return (dateDiffInDays(todaysDate, clickedDate) <= 0);
}

function testIsInThePast(Date1, Date2) {
  
  if (dateDiffInDays(new Date(Date1), new Date(Date2)) < 0) {
    return true;
  } else {
    return false;
  }
}

function isInThePast(clickedCell) {
  
  var todaysDate = new Date(getTodaysDate());
  var clickedDate = new Date(getDateForDay(clickedCell.innerText));

  if (dateDiffInDays(todaysDate, clickedDate) < 0) {
    return true;
  } else {
    return false;
  }
}

function getCurrent_LanguageUnit() {
  return document.querySelector('#lgSelected').innerText;
}

async function storeClickedReservationAsync(ev) {
  
  // REFUSE to work if user clicks on today or earlier or if there is no language unit (client) selected
  if (isInThePast(getMainDateCell(ev.target)) || getCurrent_LanguageUnit() == '') { return }

  var userSlots = await getUserSlotsPromise(ev);
  console.log("Successfully got user slots async (" + userSlots + ") Retrieving existing list item ID...");  

  var existingListItemID = await get_Existing_ListItemID(ev);
  console.log("Successfully got existing list item ID async (" + existingListItemID + "). Storing reservation async...");  

  storeClickedReservation(ev, userSlots, existingListItemID);

  /*userSlotsPromise.then(function(result){
                      console.log("Successfully got user slots async. Launching store reservation routine");  
                      storeClickedReservation(ev, result);
                      }, function(error){
                        console.log("User slots promis rejected with message: " + error);
                      })*/
}

// get user desired ot slots (1-4 or more, but only for Today+1 or 2
function getUserSlotsPromise(ev) {
  
  return new Promise(function(resolveCallback, rejectCallback){

                        var requestDetails = {
                          reqCell: ev.target,
                          reqDate: getDateForDay(reqCell.innerText, '/'),
                          reqUnit: getCurrent_LanguageUnit()
                        };

                        var uSlots = window.prompt('Reserve how many slots for ' + requestDetails.reqUnit + ' on ' + requestDetails.reqDate + '?', '1');

                        if (uSlots) {
                          if (isLegalRequest(uSlots, requestDetails)) {               // have to check whether asked for slots are within allowable limit
                              resolveCallback(uSlots)                   // (constraints: smaller than 4, smaller or equal to remaining day slots, smaller or equal to loaner availables )
                            } else {
                              rejectCallback("Illegal request: too many slots requested!");
                            }                                                       
                        } else {
                            rejectCallback("User aborted (did not choose slots number)");
                        }
  });
}

function getUnitsAvailableConnections(requestDetails) {
  var reqCellNo = requestDetails.reqCell.id.split('_')[1];
  return document.querySelector('#unitConnections_' + reqCellNo).innerText;
}

function isLegalRequest(requestedSlots, requestDetails) {

  var available_dailyUnitConn = getUnitsAvailableConnections(requestDetails);

  if (available_dailyUnitConn == 0) { return false; }

  if (requestedSlots <= available_dailyUnitConn) {

  } else {        // user MAY request more than the available connections, but ONLY in 'loaner' days: tomorrow and after tomorrow
    if (isLoanerDate(requestDetails.reqDate)) {
      if (areLoanersAvailable(requestDetails)) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }
}

async function get_Existing_ListItemID(ev) {
  
  var clickedMainDate = getMainDateCell(ev.target);
  
  var chosenDate = getDateForDay(clickedMainDate.innerText, "/");

  //return await getPrevUnitRez_Promise(lgUnit, chosenDate);
  return await get_ListItemID_Promise_forDate(chosenDate);
}

var get_Previous_RezText = function(oReservation) {

  return new Promise(function(resolveC, rejectC) {

    var clientContext = new SP.ClientContext(siteUrl);
    var oList = clientContext.get_web().get_lists().getByTitle(oReservation.ListName);
    var oListItem = oList.getItemById(oReservation.ReuseListItemID);

    clientContext.load(oListItem);

    clientContext.executeQueryAsync(function() {
      prevRezText = oListItem.get_item(oReservation.LanguageUnit);
      resolveC(prevRezText);
      console.log('get_Previous_RezText: successfully retrieved previous reservation: ' + prevRezText);
    }, function() {
      rejectC(err);
      console.error('get_Previous_RezText: error retrieving previous reservation: ' + err);
    });
  });
};

// returns Promise resolved with ID of existing list item, or -1
var get_ListItemID_Promise_forDate = function(Date) {
  
  return new Promise(function(resolveC, rejectC) {
    var clientContext = new SP.ClientContext(siteUrl);
    var reservationsList = clientContext.get_web().get_lists().getByTitle(detailedListName);

    //"<IsNotNull><FieldRef Name=\"" + Unit + "\"/></IsNotNull>" +
    var camlQuery = new SP.CamlQuery();
    camlQuery.set_viewXml(
      "<View><Query>" +
          "<Where>" +
              "<Eq><FieldRef Name=\"OTDate\"/><Value Type=\"Text\">" + Date + "</Value></Eq>" + 
          "</Where>" +
          "<OrderBy><FieldRef Name=\"OTDate\" Ascending=\"TRUE\"/></OrderBy>" +
      "</Query>"+
      "<RowLimit>5000</RowLimit>" +
      "</View>");
      var rItems = reservationsList.getItems(camlQuery);
      clientContext.load(rItems);
      
      clientContext.executeQueryAsync(function() {
        
        // No array needed, this query will retrieve exactly one item list item (or none)
        /*var itemArray = [];
        var itemEnumerator = rItems.getEnumerator();

        while(itemEnumerator.moveNext()) {
          var item = itemEnumerator.get_current();
          var otd = item.get_item("OTDate");
          var orez = item.get_item(Unit);

          itemArray.push(otd + ": " + orez);
        }
        //resolveC(itemArray);*/

        if (rItems.get_count() > 0) {
          var lIDate = rItems.getItemAtIndex(0);    // List Item for Date
          var lIDate_ID = lIDate.get_id();          // List item for Date ID
          resolveC(lIDate_ID);                                  // return the list item, if found, rather then the actual text content. 
                                                                // We'll pick this in the 'storeClickedReservationAsync' to pass it on for writing target to 'storeClickedReservation' (instead of creating a new list entry)
          console.log('get_ListItemID_forDate: ID: ' + lIDate_ID);
          /*var urez_Text = uRez_Item.get_item(Unit);
          resolveC(urez_Text);*/
        } else {
          //resolveC("");
          resolveC(-1);
          console.log('get_ListItemID_forDate: ID: ' + lIDate_ID);
        }
      }, function(err) {
        rejectC(err);
        console.error('Get Unit Reservation query returned an error:' + err);
      });
  });
};

// user may click on parent element or some sibling/ child of sibling (instead of the desired mainDate div)
function getMainDateCell(clickedElement) {
  if (clickedElement.classList.contains('mainDate')) {
    return clickedElement;
  } else {
    var elemNo = clickedElement.id.split('_')[1];
    return document.querySelector('#mainDate_' + elemNo);
  }
}

function storeClickedReservation(ev, userSlotsRequested, existingListItemID) {
  
  var clickedMainDate = getMainDateCell(ev.target);

  var oReservation = new Object();
      
      oReservation.ListName = detailedListName;   // TESTING more detailed data storage and retrieval
      oReservation.ChosenDate = getDateForDay(clickedMainDate.innerText, "/");
      oReservation.LanguageUnit = getCurrent_LanguageUnit();
      oReservation.UserName = getCurrentUserID('GetShortVersion');
      oReservation.SlotsReserved = userSlotsRequested;
      oReservation.ReuseListItemID = existingListItemID;    // could also be -1, for non-existing
      
      /*var chosenDate = clickedCell.innerText;
      var ctLanguageUnit = document.querySelector('#lgSelected').innerText;
      var recordsReserved = window.prompt('Reserve how many slots for ' + ctLanguageUnit + ' on ' + chosenDate + '?', '1');*/
      //writeOTBk_toList(oReservation);
    writeOTBk_toDetailedList(oReservation);   // TESTING MORE COMPLEX DATA STORAGE
}

async function writeOTBk_toDetailedList(oReservation) {
  
  var previousRezText = null;   // empty string
  var newReservation = oReservation.SlotsReserved + '|' + oReservation.UserName + '|' + getCurrentTime();
  //
  if (oReservation.ReuseListItemID != -1) {
    
    previousRezText = await get_Previous_RezText(oReservation);

    // even if we're using existing list item, specific language unit entry might still be missing!
    if (previousRezText != null) {
      var reservationInfo = previousRezText + ' ' + newReservation;
    } else {
      reservationInfo = newReservation;
    }
  } else {
    reservationInfo = newReservation;
  }
  oReservation.ReservationInfo = reservationInfo;       // This is all reservations done by unit on this particular day
  oReservation.RequestedReservation = newReservation;   // Also save new rez info to received reservation object, for confirmation

  // Start with writing to the existing or new list item
  var clientContext = new SP.ClientContext(siteUrl);
  var oList = clientContext.get_web().get_lists().getByTitle(oReservation.ListName);

    // Either get existing list item object to fill in, or create a new one
  if (oReservation.ReuseListItemID != -1) {
    this.oListItem = oList.getItemById(oReservation.ReuseListItemID); 
    oListItem.set_item(oReservation.LanguageUnit, reservationInfo);
  } else {
    var itemCreateInfo = new SP.ListItemCreationInformation();
    this.oListItem = oList.addItem(itemCreateInfo);
    oListItem.set_item('OTDate', oReservation.ChosenDate);
    oListItem.set_item(oReservation.LanguageUnit, reservationInfo);
  }
  
  //var prevReservations = oListItem.get_item(oReservation.LanguageUnit);
  // check to not overwrite but add to prev rev for same day same unit!
  /*if (prevReservations) {
    reservationInfo = prevReservations + ' ' + reservationInfo;
  }*/
  
  oListItem.update();
  clientContext.load(oListItem);
  //clientContext.executeQueryAsync(Function.createDelegate(this, this.onQuerySucceeded(oReservation)), Function.createDelegate(this, this.onQueryFailed));
  clientContext.executeQueryAsync(onQuerySucceeded(oReservation), onQueryFailed);
}

//
//******************************* END of Code to store current reservaton (including getting previous unit reservation) ********************************* 

// Function to get all list items which contain reservations of a particular unit (geting in fact all of one unit's current month reservation)
function getUnitRez_ItemCol(Month, Unit) {
  
  return new Promise(function(resolveC, rejectC) {

    var clientContext = new SP.ClientContext(siteUrl);
    var reservationsList = clientContext.get_web().get_lists().getByTitle(detailedListName);
    var ctYear = getCurrentYear();
    var fMonth = formatNumber(Month, '00');

    var camlQuery = new SP.CamlQuery();
    camlQuery.set_viewXml(
        "<View><Query>" +
            "<Where>" +
                "<And>" +
                    "<BeginsWith><FieldRef Name=\"OTDate\"/><Value Type=\"Text\">" + ctYear + "/" + fMonth + "</Value></BeginsWith>" + 
                    "<IsNotNull><FieldRef Name=\"" + Unit + "\"/></IsNotNull>" +
                "</And>" +
            "</Where>" +
            "<OrderBy><FieldRef Name=\"OTDate\" Ascending=\"TRUE\"/></OrderBy>" +
        "</Query>"+
        "<RowLimit>5000</RowLimit>" +
        "</View>");
    var rItems = reservationsList.getItems(camlQuery);
    clientContext.load(rItems);

    //this.uRezArray = undefined;
    /*clientContext.executeQueryAsync(function() {
                                    //alert("OT Date: " + Unit + " rez\n" + itemArray.join("\n"));
                                    console.log('Successfully retrieved ' + Unit + ' reservations for ' + Year + '/' + Month);
                                  }, function(sender,args){
                                    //alert(args.get_message());
                                    console.log(args.get_message());
                                  });*/
    
    clientContext.executeQueryAsync(function(){
      
    var itemArray = [];
    var itemEnumerator = rItems.getEnumerator();

    while(itemEnumerator.moveNext()) {
      
      var item = itemEnumerator.get_current();
      var otd = item.get_item("OTDate");
      var orez = item.get_item(Unit);

      itemArray.push(otd + "_" + orez);
    }
    resolveC(itemArray);

    }, function(err){
      rejectC(err);
    });
  }); 
}

async function getUnitsReservations(Month, Unit) {
  return await getUnitRez_ItemCol(Month, Unit);
}

async function getUnitsReservations_Array(Month, Unit) {

  var uRezDetailsPromise = await getUnitsReservations(Month, Unit);

  var placesReserved = Array(daysInCtYrMonth(Month)).fill(0);

  for (var i = 0; i < uRezDetailsPromise.length; i++) {
    var day = uRezDetailsPromise[i].split('_')[0].split('/')[2];
    var slots = getSlots(uRezDetailsPromise[i].split('_')[1]);
    placesReserved[day-1] = slots;                                // we have to write to index - 1 because the array starts at 0, the day is 1-based, 
                                                                  // since stemming from user interaction!
  }
  return placesReserved;
}

// split incoming unit's reservation text for one day and return either the slots reserved part or the sum of them, in case of multiple reservations by same unit same day
function getSlots(BookingsText) {
  if (BookingsText.includes(' ')) {
    var bks = BookingsText.split(' ');
    var slts = bks.map(function(item) {
      return item.split('|')[0];
    });
    return slts.reduce(function(sum, item) {
      return parseInt(sum) + parseInt(item);
    });
  } else {
    return parseInt(BookingsText.split('|')[0]);
  }
}

// For Occ TW the algorithm is simple: there are 4 connections per day per unit, for a total of 25 users, so 100 connectios daily.
// For Std TW though, there should be a function here returning a number of connections obtained as a percentage of the number of connections requested by that unit
// Therefore, the function should take 2 parameters: the Unit and an array of connections for the units (either a custom list, or an xml file from which to read, 
// or even an html element - like a table for instance, taken from some "settings page"). To apply this system to the StdTW, we'll just replace this below function with the new one.
function getPerUnitDailyConnections() {
  return dailyUnitConnections;
}

function getTotalUnitConnections() {
  return(languageUnits.length * parseInt(getPerUnitDailyConnections()));
}

// Function returns a very useful array of connections available for current unit for current month
async function getUnitsAvailableConnections_Array(Month, Unit) {
  var unitReservationsDone = await getUnitsReservations_Array(Month, Unit);
  var unitDailyConnections = getPerUnitDailyConnections();
  return unitReservationsDone.map(function(arrItem) {
    return parseInt(unitDailyConnections) - parseInt(arrItem);
  });
}

function getItemsOnQuerrySucceeded(sender, args) {
  var itemArray = [];
  var itemEnumerator = items.getEnumerator();
  while(itemEnumerator.moveNext()){
    var item = itemEnumerator.get_current();
    var otd = item.get_item("OTDate");
    var orez = item.get_item('RO');
    itemArray.push(otd + ": " + orez);
  }
  console.log(itemArray);
  //uRezArray = itemArray;
  //console.log('Successfully retrieved items');
}

function logOnQuerryFailed(sender, args) {
  console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
}

// compose 'standard' compact reservation-related (transaction) data to string of form: "Slots|UserID|Time", ie '1|margama|16:10' for storage in detailed reservations custom list
function packReservationData(slots) {

}

function clearCalendarHidden() {

  var dateCells = document.querySelectorAll('.dateCell');

  var cell;
  for (cell of dateCells) {
    if (cell.style.visibility == 'hidden') {
      cell.style.visibility = '';
    }
  }

  dateCells = document.querySelectorAll('.unitConnections');
  
  for (cell of dateCells) {
    if (cell.style.visibility == 'hidden') {
      cell.style.visibility = '';
    }
  }

  dateCells = document.querySelectorAll('.spareConnections');
  
  for (cell of dateCells) {
    if (cell.style.visibility == 'hidden') {
      cell.style.visibility = '';
    }
  }
}

function clearCalendarWeekEnds() {
  
  var weekEndCells = document.querySelectorAll('.' + weekEndMarkerClass);

  var cell;
  for (cell of weekEndCells) {
    if (cell.classList.contains(weekEndMarkerClass)) {
      cell.classList.remove(weekEndMarkerClass);
    }
  }
}

function getMonthDetails(month) {

  var resultObject = {firstDay: 0, daysInMonth: 0};

  // As used by default, without the 'month' parameter, 
  if (month == undefined || month == null) {
    resultObject.firstDay = get_CtMonth_FirstDay();     // 1-based week day of current month
    resultObject.daysInMonth = daysInCtMonth();         // Number of days in current month
  } else {
    resultObject.firstDay = get_Month_FirstDay(month);
    resultObject.daysInMonth = daysInCtYrMonth(month);             // Number of days in current month
  }
  if (resultObject.firstDay == 0) { resultObject.firstDay = 7 };         // RO style hacking back to reality.. Sunday comes on 7th place!

  return resultObject;
}

// building the real calendar form the inert table on the page
function populateCalendar(month) {

  // change month & year labels
  displayCalendarMonth(month);
  
  // get requested month's first day of week & days in month
  var monthDetails = getMonthDetails(month);

  // and write days accordingly
  writeCalendarForMonth(monthDetails.firstDay, monthDetails.daysInMonth)

  // and if we are displaying current month, mark today with color/ otherwise clear it
  markToday();
  
  // compute and write to table the available unit connections (4 - the reserved ones for OccTw, needs more work for PermTW)
  writeUnits_AvailableConnections(month);
  
  // this function needs to find tommorow & after tomorrow and display loaner connections for those days after it computes them
  // TODO: functions 'getTomorrowAndAfterTomorrow', 'getAvailableSpareConnections', 'write_Available_SpareConnections'
  //write_Available_SpareConnections(month);

  // attempt table redraw to solve missing borders on cells neighbouring hidden ones
  forceRedraw(document.getElementById('monthOTCalendar'));
}


var forceRedraw = function(element){
  var disp = element.style.display;
  element.style.display = 'none';
  var trick = element.offsetHeight;
  element.style.display = disp;
}

function writeCalendarForMonth(firstDay, daysInMonth) {
  
  // clear calendar table days before 1st of month
  var k;
  if (firstDay != 1) {
    for (k = 1; k < firstDay; k++) {
      var targetCell = document.querySelector('#dateCell_' + k);
      var targetMainDate = document.querySelector('#mainDate_' + k);
      // clearing a day's cell also needs clearing the unit's availables & loaners
      targetMainDate.innerText = '';

      if (!targetCell.classList.contains(weekEndMarkerClass)) {
        document.querySelector('#unitConnections_' + k).innerText = '';
        document.querySelector('#spareConnections_' + k).innerText = '';  
      }
      targetCell.setAttribute("invisible", "true");
    }
  }

  // write proper month days
  var i, j;                                                         // j - day in month
  // i needs to start at 1 and go until end of table, at 37. 
  // For items less than firstDay, clear cells, same for cells from daysInMonth + firstDay onward                                                 
  for (i = firstDay, j = 1; i < daysInMonth + firstDay; i++, j++) {
    var targetCell = document.querySelector('#dateCell_' + i);
    var targetMainDate = document.querySelector('#mainDate_' + i);
    targetMainDate.innerText = j;

    // re-deploy invisible cells from previous months (user scrolling)
    if (targetCell.getAttribute('invisible') == 'true') { targetCell.setAttribute('invisible', 'false')}

    if (!targetCell.classList.contains(weekEndMarkerClass)) {                   // only attempt to fill-in unit & spare connections for non-weeend days
      //document.querySelector('#unitConnections_' + i).innerText = '4';
      //document.querySelector('#spareConnections_' + i).innerText = '+96';
    }
  }

  // clear eventual entries from end of calendar, left over from other months
  var l;
  for (l = daysInMonth + firstDay; l < 43; l++) {
      var targetCell = document.querySelector('#dateCell_' + l);
      var targetMainDate = document.querySelector('#mainDate_' + l);
      targetMainDate.innerText = '';

      if (!targetCell.classList.contains(weekEndMarkerClass)) {
        document.querySelector('#unitConnections_' + l).innerText = '';
        document.querySelector('#spareConnections_' + l).innerText = '';
      }
      targetCell.setAttribute("invisible", "true");
  }
}

async function writeUnits_AvailableConnections(month) {

  var ctUnit = getCurrent_LanguageUnit();

  if (ctUnit == "") { return }

  if (!month) { 
    var ctMonth = getCurrentMonth() + 1;
   } else {
     ctMonth = month;
   }
  var unitsAvailableConnections = await getUnitsAvailableConnections_Array(ctMonth, ctUnit);
  
  var mOffset = getMonthDetails(ctMonth).firstDay;

  for (var i = 0; i < unitsAvailableConnections.length; i++) {
    var tgCell = document.querySelector('#unitConnections_' + (i + mOffset));
    if (tgCell) { tgCell.innerText = unitsAvailableConnections[i]; }  
  }
}

function cleanDivs() {
  var zws = new RegExp(String.fromCharCode(8203),"g");
  tgDivs = document.querySelectorAll('.mainDate');
  for (let d of tgDivs) {
    d.innerHTML = d.innerHTML.replace("&nbsp;", "");
    d.innerHTML = d.innerHTML.replace(zws, "");
  }
  tgDivs = document.querySelectorAll('.unitConnections');
  for (let d of tgDivs) {
    d.innerHTML = d.innerHTML.replace("&nbsp;", "");
    d.innerHTML = d.innerHTML.replace(zws, "");
  }
  tgDivs = document.querySelectorAll('.spareConnections');
  for (let d of tgDivs) {
    d.innerHTML = d.innerHTML.replace("&nbsp;", "");
    d.innerHTML = d.innerHTML.replace(zws, "");
  }
  tgDivs = document.querySelectorAll('.otConnections');
  for (let d of tgDivs) {
    d.innerHTML = d.innerHTML.replace("&nbsp;", "");
    d.innerHTML = d.innerHTML.replace(zws, "");
  }
  tgDivs = document.querySelectorAll('#monthOTCalendar td');
  for (let d of tgDivs) {
    d.innerHTML = d.innerHTML.replace("&nbsp;", "");
    d.innerHTML = d.innerHTML.replace(zws, "");
  }
}

function attach_MonthChange_Event() {
  
  var nextMonthButton = document.getElementById('nextMonthButton');

  nextMonthButton.addEventListener('click', function(ev) {
                                              changeCalendarMonth(ev);
                                            }
                                  );

  var prevMonthButton = document.getElementById('prevMonthButton');

  prevMonthButton.addEventListener('click', function(ev) {
                                              changeCalendarMonth(ev);
                                            }
                                  );
}

function changeCalendarMonth(ev) {

  var clickedButton = ev.target;
  var dMonth = getDisplayedMonth() + 1;
  //var ctMonth = getCurrentMonth();

  if (clickedButton.classList.contains('arrow-left')) {
    populateCalendar(dMonth - 1);
  } else {
    populateCalendar(dMonth + 1);
  }

}

function getDisplayedMonth(returnText) {

  var dMonth = document.querySelector('#otb_Month').innerText;

  if (typeof(returnText) == 'undefined' || returnText == null) {
    return parseInt(new Date(dMonth + ' 2019').getMonth());   // return 1-based month, as we use all over the place!
  } else {
    return dMonth;
  }
}

// Mouse hover event handler to display tooltip invitation (to click cell to reserve)
// Carefull not to attach it to .weekEndDay cells
function display_Reserve_InvitationTip(ev) {
  
  var clickedElement = ev.target;

  if (isInThePast(getMainDateCell(clickedElement))) { return }

  var toBeDisplayedTip = document.querySelector('#' + clickedElement.id + ' .rezTooltip');

  toBeDisplayedTip.style.visibility = 'visible';

}

function hide_Reserve_InvitationTip(ev) {
  
  var clickedElement = ev.target;

  if (isInThePast(getMainDateCell(clickedElement))) { return }

  var toBeDisplayedTip = document.querySelector('#' + clickedElement.id + ' .rezTooltip');

  toBeDisplayedTip.style.visibility = 'hidden';

}

//****************************************************************** OLD STUFF, PROBABLY TO REMOVE ********************************************************************************* */
//
// PROBABLY WONT USE TEXT FILES... TO REMOVE EVENTUALLY
function createTxtfile(resultpanel) {
  
  var clientContext;
  var oWebsite;
  var oList;
  var fileCreateInfo;
  var fileContent;

  clientContext = new SP.ClientContext.get_current();
  oWebsite = clientContext.get_web();
  oList = oWebsite.get_lists().getByTitle("Documents");  
  
  fileCreateInfo = new SP.FileCreationInformation();
  fileCreateInfo.set_url("my new file.txt");  
  fileCreateInfo.set_content(new SP.Base64EncodedByteArray());
  
  fileContent = "The content of my new file";  
  
  for (var i = 0; i < fileContent.length; i++) {
    fileCreateInfo.get_content().append(fileContent.charCodeAt(i));
  }

  this.newFile = oList.get_rootFolder().get_files().add(fileCreateInfo);
  
  clientContext.load(this.newFile);
  
  clientContext.executeQueryAsync(
    Function.createDelegate(this, successHandler),
    Function.createDelegate(this, errorHandler)
  );

  function successHandler() {
    resultpanel.innerHTML = "Go to the " + " <a href='../Documents'> Documents library</a> " + "to see your new file.";
  }

  function errorHandler() {
    resultpanel.innerHTML = "Request failed: " + arguments[1].get_message();
  }
} 

function writeOTBk_toList(oReservation) {
    
  var clientContext = new SP.ClientContext(siteUrl);
  var oList = clientContext.get_web().get_lists().getByTitle(oReservation.ListName);

  var itemCreateInfo = new SP.ListItemCreationInformation();
  this.oListItem = oList.addItem(itemCreateInfo);

  oListItem.set_item('OTDate', oReservation.ChosenDate);
  oListItem.set_item('Reservations', oReservation.LanguageUnit + ':' + oReservation.SlotsReserved);
  
  // IF NEEDED, anonymise the list item entry with the below code
  //SPuser = clientContext.get_web().EnsureUser('1073741823;#System Account');
  //1073741823;#System Account                                // User name values are in this format ID;#Login Name
  //value1 = SPuser.ID + ";#" + SPuser.Name;                  // Create in same format
  //oListItem["Author"] = value1;                             // for 'Created By' field
  //oListItem["Editor"] = value1 ;                            // for 'Modified By' field

  oListItem.update();
  clientContext.load(oListItem);

  clientContext.executeQueryAsync(Function.createDelegate(this, this.onQuerySucceeded), Function.createDelegate(this, this.onQueryFailed));
}

// returns the eventual existing actual listItem object rather than its text contents. More flexible & useful so we can reuse existing list entries, not create others
var getPrevUnitRez_Promise = function(Date) {

  return new Promise(function(resolveC, rejectC) {
    var clientContext = new SP.ClientContext(siteUrl);
    var reservationsList = clientContext.get_web().get_lists().getByTitle(detailedListName);

    //"<IsNotNull><FieldRef Name=\"" + Unit + "\"/></IsNotNull>" +
    var camlQuery = new SP.CamlQuery();
    camlQuery.set_viewXml(
      "<View><Query>" +
          "<Where>" +
              "<Eq><FieldRef Name=\"OTDate\"/><Value Type=\"Text\">" + Date + "</Value></Eq>" + 
          "</Where>" +
          "<OrderBy><FieldRef Name=\"OTDate\" Ascending=\"TRUE\"/></OrderBy>" +
      "</Query>"+
      "<RowLimit>5000</RowLimit>" +
      "</View>");
      var rItems = reservationsList.getItems(camlQuery);
      clientContext.load(rItems);
      
      clientContext.executeQueryAsync(function() {
        
        // No array needed, this query will retrieve exactly one item list item (or none)
        /*var itemArray = [];
        var itemEnumerator = rItems.getEnumerator();

        while(itemEnumerator.moveNext()) {
          var item = itemEnumerator.get_current();
          var otd = item.get_item("OTDate");
          var orez = item.get_item(Unit);

          itemArray.push(otd + ": " + orez);
        }
        //resolveC(itemArray);*/

        if (rItems.get_count() > 0) {
          var uRez_Item = rItems.getItemAtIndex(0);
          resolveC(uRez_Item);                                  // return the list item, if found, rather then the actual text content. 
                                                                // We'll pick this in the 'storeClickedReservationAsync' to pass it on for writing target to 'storeClickedReservation' (instead of creating a new list entry)
          console.log('Get Unit Reservation query returned successfully with message: ' + uRez_Item);
          /*var urez_Text = uRez_Item.get_item(Unit);
          resolveC(urez_Text);*/
        } else {
          //resolveC("");
          resolveC(null);
        }
      }, function(err) {
        rejectC(err);
        console.error('Get Unit Reservation query returned an error:' + err);
      });
  });
};

async function getRemaining_SpareConnections(Date) {
  var reservations_Array = await getReservationsForDate_Promise(Date);
  var reservations_ArrayU = reservations_Array.slice(1);
  var slots_Reserved_Array = reservations_ArrayU.map(function(i) { 
    if (i.includes(' ')) {
      var slots = i.split(' ').map( function(j) { return j.split('|')[0]; } );
      return slots.reduce( function(t, k) { return parseInt(t) + parseInt(k); } )
    } else {
      return i.split('|')[0];
    }
  });
  var slotsTotal = slots_Reserved_Array.reduce(function(t, i) { return parseInt(t) + parseInt(i); });
  return getTotalUnitConnections() - slotsTotal;
}

async function getReservationsForDate_Promise(Date) {
  
  var reservDay_listItem = await getReservationsForDate(Date);
  var result = [];
  result.push(reservDay_listItem.get_item('OTDate'));

  for (var i = 0; i < languageUnits.length; i++) {
    if (reservDay_listItem.get_item(languageUnits[i]) != null) {
      result.push(reservDay_listItem.get_item(languageUnits[i]));
    }
  }
  return result;
}

var getReservationsForDate = function(Date) {

  return new Promise(function(resolveC, rejectC) {
    var clientContext = new SP.ClientContext(siteUrl);
    var reservationsList = clientContext.get_web().get_lists().getByTitle(detailedListName);

    var camlQuery = new SP.CamlQuery();
    camlQuery.set_viewXml(
      "<View><Query>" +
          "<Where>" +
              "<Eq><FieldRef Name=\"OTDate\"/><Value Type=\"Text\">" + Date + "</Value></Eq>" + 
          "</Where>" +
          "<OrderBy><FieldRef Name=\"OTDate\" Ascending=\"TRUE\"/></OrderBy>" +
      "</Query>"+
      "<RowLimit>5000</RowLimit>" +
      "</View>");
      var rItems = reservationsList.getItems(camlQuery);
      clientContext.load(rItems);
      
      clientContext.executeQueryAsync(function() {

        if (rItems.get_count() > 0) {
          var lItem = rItems.getItemAtIndex(0);
          resolveC(lItem);                                   
          console.log('Get reservations for day query returned successfully with message: ' + lItem);
        } else {
          resolveC(null);
          console.log('Get reservations for day query found NO entry for date ' + Date);
        }
      }, function(err) {
        rejectC(err);
        console.error('Get reservations for day query returned an error:' + err);
      });
  });
};

// MS article: https://docs.microsoft.com/en-us/previous-versions/office/developer/sharepoint-2010/hh185011(v%3Doffice.14)
function createListItem(ListName) {

  var clientContext = new SP.ClientContext(siteUrl);
  var oList = clientContext.get_web().get_lists().getByTitle(ListName);

  var itemCreateInfo = new SP.ListItemCreationInformation();
  this.oListItem = oList.addItem(itemCreateInfo);

  oListItem.set_item('Title', 'My New Item!');
  oListItem.set_item('Body', 'Hello World!');

  oListItem.update();
  clientContext.load(oListItem);

  clientContext.executeQueryAsync(Function.createDelegate(this, this.onQuerySucceeded), Function.createDelegate(this, this.onQueryFailed));
}

// PROBABLY WONT USE TEXT FILES... TO REMOVE EVENTUALLY
/*function writeTextFilesSP() {
  
  var clientContext;  
  var oWebsite;  
  var oList;  
  var fileCreateInfo;  
  var fileContent;

  foreach (SP.Client.File f in files)
            {
                Console.WriteLine(f.Name);
                if (f.Name == "test1.txt")
                {
                    FileInformation fileInformation = Microsoft.SharePoint.Client.File.OpenBinaryDirect(context, (string)f.ServerRelativeUrl);
                    using (System.IO.StreamReader sr = new System.IO.StreamReader(fileInformation.Stream))
                    {
                        // Read the stream to a string, and write the string to the console.
                        String line = sr.ReadToEnd();
                        Console.WriteLine(line);
                    }

                    using (System.IO.StreamWriter wr = new System.IO.StreamWriter(fileInformation.Stream))
                    {
                        // Read the stream to a string, and write the string to the console.
                        wr.Write("HI");
                       // Console.WriteLine(line);
                    }

                }
            }
}*/
//
//****************************************************** END of OLD STUFF, PROBABLY TO REMOVE *********************************************************************** */





// SUCCESS OR FAILURE EVENT HANDLERS, TO CUSTOMIZE PROPERLY
function onQuerySucceeded(oReservation) {
  //alert('Booking created: ' + oListItem.get_id());
  alert('Booking created (' + oReservation.ChosenDate + " " + oReservation.LanguageUnit + ": " + oReservation.RequestedReservation + ")");
}

function onQueryFailed(sender, args) {
  alert('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
}
//
//****************************************************** END RESERVATION CODE ************************************************************************************** */

//****************************************************** EXPERIMENTAL CODE ***************************************************************************************** */
//****************************************************** USER MANAGEMENT ? ***************************************************************************************** */

// Test function to log current user's data
function logCurrentUser() {

  var context = SP.ClientContext.get_current();
  var web = context.get_web();
  var user = web.get_currentUser(); //must load this to access info.
  context.load(user);
  context.executeQueryAsync(function(){
                                console.log("User is: " + user.get_title() + " " + user.get_loginName() + " " + user.get_email()); //there is also id, email, so this is pretty useful.
                            }, function(){
                                console.log(":(");
                                });
}

function getCurrentUserID(getShortVersion) {
  var context = SP.ClientContext.get_current();
  var web = context.get_web();
  var user = web.get_currentUser(); //must load this to access info.
  context.load(user);
  context.executeQueryAsync(function(){
                                console.log("getCurrentUserID: SP User ID successfully retrieved!");   //there is also id, email, so this is pretty useful.
                            }, function(){
                                console.log("getCurrentUserID: Error, could not retrieve SP user ID!");
                                });
  
  if (getShortVersion) {
    return user.get_loginName().split('|')[1].split('\\')[1];
  } else {
    return user.get_loginName();
  }
}

function testSPCreateTextFile() {
  createTxtfile(document.querySelector('#resultpanel'));
}

function testTaffyDB() {
  
  var taffyTestDB = TAFFY();

  taffyTestDB.insert([{name:"New York",state:"WA"},{name:"Las Vegas",state:"NV"},{name:"Boston",state:"MA"}]);

  alert(taffyTestDB({name:"Boston"}).count());

}


//************************************************************ ONLOAD MAIN FUNCTIONS (window's load event handlers) ************************************************/
function addOnLoad(newOnload) {
  var lastOnload = window.onload;
  window.onload = function() {
    if (lastOnload) lastOnload();
    newOnload();
  }
}

// Save user's language unit on current machine
function RecogniseUsersLanguageSelection() {

  var previousLangSelection = localStorage.getItem("OTB_SelectedLanguage");
  if (previousLangSelection) {
    document.querySelector("#lgSelected").innerText = previousLangSelection;
  }
}

function displayCalendarMonth(month) {
  
  if (month == undefined || month == null) {
    document.querySelector('#otb_Month').innerText = getCurrentMonth('ReturnText');
    document.querySelector('#otb_Year').innerText = getCurrentYear();
  } else {
    document.querySelector('#otb_Month').innerText = getMonthName(parseInt(month) - 1);
    document.querySelector('#otb_Year').innerText = getCurrentYear();
  }
}

//**************************************************************************************************************************************************************** */

// for functions which we need launched when window has finished loaded completely, lets use a chain of onload events:
// Just use the addOnLoad function to send in any functions you wish executed at that time. As you may see below, the function 
// does not replace the eventual existing onload event of the window, it executes it and adds the new one.



/* ***************************************************** EXECUTABLE CODE ********************************************************* */

// First clean garbage spaces
addOnLoad(cleanDivs);

// Choose once, remember user selection until next eventual selection?
addOnLoad(RecogniseUsersLanguageSelection);

// Display current month and year in calendar header position
addOnLoad(displayCalendarMonth);

// Retrieve and attach click event for all language links in the language selector (so user can set his unit's language)
addOnLoad( function() {
  return attach_LanguageSelect_Event("ulLink");
});

// write this month's calendar
addOnLoad(populateCalendar); 

// find next two working days and write loaner connections remaining
addOnLoad(writeLoanerConnections);

// Retrieve all calendar days (table cells) and attach click event - so user may start a reservation
addOnLoad( function() {
  return attach_BookOTw_ClickEvent("dateCell");
});

addOnLoad( function() {
  return attach_MonthChange_Event();
});

addOnLoad( function() {
  return attach_ShowReserve_Tooltip_MouseoverEvent();
})

/* *************************************************** END EXECUTABLE CODE ********************************************************** */




// Mark current day in calendar
//addOnLoad(markToday);

// Mark week-end days
//addOnLoad(markCtWeekEnds);

//addOnLoad(testSPCreateTextFile);

//addOnLoad(createListItem);

/*addOnLoad( function() {
  return identLinks_AndAttachNavEvent("unhideLink");
});*/

/*addOnLoad(finishNavigateToHidden);*/

function tryFileSaver() {
  //var FileSaver = require('file-saver');
  //import {saveAs} from 'FileSaver.js';
  var blob = new Blob(["Hello, world!"], {type: "text/plain;charset=utf-8"});
  saveAs(blob, "\\collaboration.consilium.eu.int\collab\ts\cat\otbk\Assets\js\hello world.txt");
}